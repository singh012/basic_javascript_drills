function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let counter = 3;
    let obj = {

        increment() {
            counter++
            //console.log(`counter value is ${counter}`)
        },

        decrement() {
            counter--
            //console.log(`counter value is ${counter}`)
        }
    }
    return obj
}

let result = counterFactory()
result.increment()
result.increment()
result.decrement()
result.decrement()


function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    return () => {
        return () => {
            if (n > 0) {
                n--
                cb(n)
            } else {
                return null
            }
        }
    }
}

let resultOfLimitFun = limitFunctionCallCount((n) => {
    console.log('Hello', n);
}, 5)();

resultOfLimitFun();
resultOfLimitFun();
resultOfLimitFun();
resultOfLimitFun();



function cacheFunction(cb) {
    // Should return a funciton that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    const obj = {}

    return innerFunction = (...args) => {
        if (obj[args]) {
            return obj[args]
        } else {
            obj[args] = args
            return cb(...args)
        }
    }

}

let cacheResult = cacheFunction((...args) => console.log(args))


cacheResult('Hello', 'Hi')
cacheResult('Hello', 'Hi')
cacheResult('Hello', 'Hii', 'Hi', 'Hey')


