const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    return Object.keys(obj)
}

let keysResult = keys(testObject)

//console.log(keysResult)

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    return Object.values(obj)
}

let valuesResult = values(testObject)

//console.log(valuesResult)

function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    for (let key in obj) {
        if (Number(obj[key])) {
            obj[key] = cb(obj[key])
        }
    }
    return obj
}

let mapObjectResult = mapObject(testObject, (val) => val += 1)

// console.log(mapObjectResult)


function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    return Object.entries(obj)
}

let pairsResult = pairs(testObject)

//console.log(pairsResult)

/* STRETCH PROBLEMS */

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    let returnObj = {}

    for (let key in obj) {
        if (!returnObj[obj[key]]) {
            returnObj[obj[key]] = key
        }
    }

    return returnObj
}

let invertResult = invert(testObject)

//console.log(invertResult)


let defaultProps = { location: 'Hyderabad' };

function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    for (keyFirst in obj) {
        for (let keySecond in defaultProps) {
            if (keyFirst === keySecond) {
                obj[keyFirst] = defaultProps[keySecond]
            }
        }
    }

    return obj
}

let defaultReuslt = defaults(testObject, defaultProps)

//console.log(defaultReuslt)